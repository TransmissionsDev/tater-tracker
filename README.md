# [TaterTracker](https://tater-tracker.now.sh) &middot; ![Now](https://badgen.net/badge/Deployed%20with/Now/black?icon=now)

A simple React app to track your dog's bathroom history and walks.

|                     Log a Walk                      |                   View past walks                   |
| :-------------------------------------------------: | :-------------------------------------------------: |
| ![Log a walk Page](https://i.imgur.com/GK3zB16.png) | ![Past walks page](https://i.imgur.com/ibgyyHW.png) |

- **Simple:** Interface is simple and fast. You have no excuse not to track your dog's history if it's this easy!

- **Offline Support:** Since TaterTracker uses `create-react-app` as a boilerplate generator, TaterTracker uses [Google Workbox](https://developers.google.com/web/tools/workbox) and has a built in Service Worker to cache the app to your device and enable fast loading and offline support! At 10,000 feet in the air or 100 feet under, you can log your dog's poops like a champ!

- **PWA:** You can add TaterTracker to your homescreen and get a near native expirence.

## Host your own

[![Deploy with ZEIT Now](https://zeit.co/button)](https://zeit.co/new/project?template=https://github.com/TransmissionsDev/tater-tracker)

## Coming soon

- **Firebase Database & Authentication:** Soon TaterTracker will integrate with Firebase Realtime Database & Authentication so the whole family can be on the same page and keep randoms from messing with your logs!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
