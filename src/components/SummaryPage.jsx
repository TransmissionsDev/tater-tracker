import React, { Component } from "react";
import PropTypes from "prop-types";

import Table from "react-bootstrap/Table";

import Moment from "moment";

export default class SummaryPage extends Component {
  static propTypes = {
    history: PropTypes.array.isRequired
  };

  render() {
    const { history } = this.props;

    return (
      <Table hover>
        <thead>
          <tr>
            <th>Time:</th>
            <th>Type:</th>
          </tr>
        </thead>
        <tbody>
          {history.map(walk => (
            <tr key={walk.time}>
              <td>{Moment(walk.time).calendar()}</td>
              <td>{walk.type}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}
