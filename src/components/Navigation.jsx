import React, { Component } from "react";
import PropTypes from "prop-types";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import Logo from "../assets/logo50.png";

export default class Navigation extends Component {
  static propTypes = {
    onPageSelect: PropTypes.func.isRequired,
    activePage: PropTypes.string.isRequired,
    walkCount: PropTypes.number.isRequired
  };

  render() {
    const { onPageSelect, activePage, walkCount } = this.props;

    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>
          <img
            alt=""
            src={Logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          TaterTracker
        </Navbar.Brand>
        <Nav onSelect={onPageSelect} activeKey={activePage} variant="pills">
          <Nav.Link href="#logwalk">Log Walk</Nav.Link>

          <Nav.Link href="#summary" disabled={walkCount > 0 ? false : true}>
            History
          </Nav.Link>
        </Nav>
      </Navbar>
    );
  }
}
