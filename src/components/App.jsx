import React, { Component } from "react";

import Navigation from "./Navigation";
import LogWalkPage from "./LogWalkPage";
import SummaryPage from "./SummaryPage";

export default class App extends Component {
  state = {
    page: "#logwalk",
    history: []
  };

  onBathroomUse = type => {
    this.setState(state => ({
      history: [...state.history, { type, time: Date.now() }]
    }));
  };

  onPageSelect = page => {
    this.setState({ page: page });
  };

  render() {
    const { page, history } = this.state;

    return (
      <>
        <Navigation
          onPageSelect={this.onPageSelect}
          activePage={page}
          walkCount={history.length}
        />

        {page === "#logwalk" ? (
          <LogWalkPage onBathroomUse={this.onBathroomUse} />
        ) : (
          <SummaryPage history={history} />
        )}
      </>
    );
  }
}
