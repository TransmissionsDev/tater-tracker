import React, { Component } from "react";
import PropTypes from "prop-types";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

import TaterLeft from "../assets/tater-left.png";
import TaterRight from "../assets/tater-right.png";

import Swal from "sweetalert2";

export default class LogWalkPage extends Component {
  static propTypes = {
    onBathroomUse: PropTypes.func.isRequired
  };

  onClick = event => {
    const bathroomType = event.target.alt;

    this.props.onBathroomUse(bathroomType);

    Swal.fire({
      title: `Logged a ${bathroomType}`,
      icon: "success",
      showConfirmButton: false,
      width: 300,
      timer: 1000
    });
  };

  render() {
    return (
      <Container>
        <br />
        <Row className="justify-content-md-center">
          <Col md="auto">
            <Card>
              <Card.Body>
                <img
                  width={"50%"}
                  className="tater-image"
                  alt="Pee"
                  onClick={this.onClick}
                  src={TaterLeft}
                />
                <img
                  width={"50%"}
                  className="tater-image"
                  onClick={this.onClick}
                  alt="Poop"
                  src={TaterRight}
                />
              </Card.Body>
            </Card>
            <br />
            <Card>
              <Card.Body>
                <h3 style={{ textAlign: "center", margin: 0 }}>
                  Click one of the buttons to log a poop/pee!
                </h3>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
